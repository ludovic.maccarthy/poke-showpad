import { ReactChild } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { mockPokemonResponse } from '../../mocks/pokemonMock';
import { CardInterface } from '../components/Card/Card';

export const API_POKEMON_ENDPOINT = 'https://pokeapi.co/api/v2/pokemon/';
export const API_SPECIE_ENDPOINT = 'https://pokeapi.co/api/v2/pokemon-species/';

export const buildPokemons = (count: number): CardInterface[] => {
    let i = 0;
    const pokemons = [];
    for (i === 0; i < count; i++) {
        const initialPokemonState: CardInterface = { id: 0 + i, name: 'Pokemon ' + i, url: 'URL ' + i, bg: 'bg ' + i };
        pokemons.push(initialPokemonState);
    }
    return pokemons;
};

export const mockCustomPokemon = (params = {}) => {
    return { ...mockPokemonResponse, ...params };
};

export const defaultStore = {
    api: {
        pokemons: {
            page: 1,
            count: 10,
            results: buildPokemons(100),
            type: 'pokemons',
        },
        search: {
            page: 1,
            count: 20,
            results: buildPokemons(100),
            type: 'search',
        },
        status: 'waiting',
        actual_search: 'test',
        page: 1,
    },
    favs: {
        page: 1,
        results: buildPokemons(4),
        count: 4,
        type: 'favorites',
    },
    collection: {
        page: 1,
        results: buildPokemons(4),
        count: 4,
        type: 'collection',
    },
};

export const mockedStore = () => {
    const mockStore = configureStore();
    const store = mockStore(defaultStore);
    return store;
};

export const emptyStore = () => {
    const mockedStore = {
        api: {
            pokemons: {
                page: 1,
                count: 0,
                results: [],
                type: 'test',
            },
            search: {
                page: 1,
                count: 0,
                results: [],
                type: 'test',
            },
            status: 'waiting',
            actual_search: '',
            page: 1,
        },
        favs: {
            page: 1,
            results: [],
            count: 0,
            type: 'favorites',
        },
        collection: {
            page: 1,
            results: [],
            count: 0,
            type: 'collection',
        },
    };

    const mockStore = configureStore();
    const store = mockStore(mockedStore);
    return store;
};

export const wrapWithProviderAndRouter = (child: ReactChild): JSX.Element => {
    return (
        <Provider store={mockedStore()}>
            <BrowserRouter>{child}</BrowserRouter>
        </Provider>
    );
};
