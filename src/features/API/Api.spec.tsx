import * as API from './Api';
import axios from 'axios';

const getPokemonsEndpoint = 'https://pokeapi.co/api/v2/pokemon?limit=10000&offset=0';
const getPokemonDetailEndpoint = 'https://pokeapi.co/api/v2/pokemon/';
const getPokeDetailsByNameEndpoint = 'https://pokeapi.co/api/v2/pokemon-species/';

jest.mock('axios', () => ({
    get: jest.fn(),
}));

const spyAxios = jest.spyOn(axios, 'get');

describe('Api helper', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });
    test('getPokemons', () => {
        API.getPokemons();
        expect(spyAxios).toHaveBeenCalledWith(getPokemonsEndpoint);
    });
    test('getPokemonById', () => {
        const pokemonId = 1;
        API.getPokemonById(pokemonId);
        expect(spyAxios).toHaveBeenCalledWith(getPokemonDetailEndpoint + pokemonId);
    });
    test('getPokeDetailsByName', () => {
        const pokemonName = 'name';
        API.getPokeDetailsByName(pokemonName);
        expect(spyAxios).toHaveBeenCalledWith(getPokeDetailsByNameEndpoint + pokemonName);
    });
});
