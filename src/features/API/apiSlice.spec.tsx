import axios from 'axios';
import { store } from '../../app/store';
import { CardInterface } from '../../components/Card/Card';
import { pokeResultsInitialState } from '../../components/List/List';
import { buildPokemons } from '../../helpers/helpers';
import {
    getPokemonsAsync,
    importPokemons,
    lookForPokemon,
    setActualSearch,
    setStatus,
    updatePage,
    updatePokemon,
    updateSearch,
    updateSearchPage,
} from './apiSlice';

const initialState = {
    pokemons: pokeResultsInitialState,
    search: pokeResultsInitialState,
    status: 'waiting',
    actual_search: '',
    page: 1,
};

const spyStorage = jest.spyOn(Storage.prototype, 'getItem');
let mockPokemons: CardInterface[];
describe('apiSlice', () => {
    describe('getPokemonsAsync', () => {
        test('getPokemonsAsync', () => {
            store.dispatch(getPokemonsAsync());
            const state = store.getState().api;
            expect(state).toEqual({ ...initialState, status: 'pending' });
        });
        test('getPokemonsAsync fail', async () => {
            console.log = jest.fn();
            const spyAxios = jest.spyOn(axios, 'get');
            spyAxios.mockRejectedValue('test');
            await store.dispatch(getPokemonsAsync());
            const state = store.getState().api;
            expect(state.status).toBe('error');
            expect(console.log).toHaveBeenCalledWith('API CALL ERROR:', 'test');
        });
    });
    test('setStatus', () => {
        store.dispatch(setStatus());
        const changedState = store.getState().api;
        expect(changedState).toEqual({ ...changedState, status: 'ok' });
    });
    test('setActualSearch', () => {
        store.dispatch(setActualSearch('test'));
        const changedState = store.getState().api;
        expect(changedState).toEqual({ ...changedState, actual_search: 'test' });
    });

    describe('importPokemons', () => {
        afterEach(() => {
            jest.clearAllMocks();
        });
        test('importPokemons with empty response', () => {
            spyStorage.mockImplementation(() => JSON.stringify(buildPokemons(0)));
            store.dispatch(importPokemons());
            const state = store.getState().api;
            expect(spyStorage).toHaveBeenCalled();
            expect(state).toEqual({ ...state, status: 'filled' });
            expect(state.pokemons.count).toEqual(0);
        });
        test('importPokemons with filled response', () => {
            const spyJsonParse = jest.spyOn(JSON, 'parse');
            const mockPokemons = buildPokemons(200);
            spyStorage.mockImplementation(() => JSON.stringify(mockPokemons));
            store.dispatch(importPokemons());
            const state = store.getState().api;
            expect(spyStorage).toHaveBeenCalled();
            expect(state).toEqual({ ...state, status: 'filled' });
            expect(state.pokemons.results).toEqual(mockPokemons);
            expect(state.pokemons.count).toEqual(mockPokemons.length);
            expect(spyJsonParse).toHaveBeenCalled();
        });
    });

    describe('test lookForPokemon', () => {
        beforeEach(() => {
            mockPokemons = buildPokemons(200);
            spyStorage.mockImplementation(() => JSON.stringify(mockPokemons));
            store.dispatch(importPokemons());
        });
        test('lookForPokemon with empty result', () => {
            store.dispatch(lookForPokemon({ search: 'Test' }));
            const state = store.getState().api;
            expect(state.actual_search).toBe('Test');
            expect(state.search.results.length).toBe(0);
        });
        test('lookForPokemon with results', () => {
            store.dispatch(lookForPokemon({ search: mockPokemons[111].name }));
            const state = store.getState().api;
            expect(state.actual_search).toBe(mockPokemons[111].name);
            expect(state.search.results.length).toBe(1);
        });
    });

    describe('updateSearchPage', () => {
        beforeAll(() => {
            mockPokemons = buildPokemons(200);
            spyStorage.mockImplementation(() => JSON.stringify(mockPokemons));
            store.dispatch(importPokemons());
            store.dispatch(lookForPokemon({ search: mockPokemons[1].id }));
        });
        test('updateSearchPage + 1', () => {
            const searchPage = store.getState().api.search.page;
            store.dispatch(updateSearchPage({ page: searchPage + 1 }));
            const updatedSearchPage = store.getState().api.search.page;
            expect(updatedSearchPage).toBe(searchPage + 1);
            // console.log(store.getState().api);
        });
        test('updateSearchPage -1', () => {
            const searchPage = store.getState().api.search.page;
            store.dispatch(updateSearchPage({ page: searchPage - 1 }));
            const updatedSearchPage = store.getState().api.search.page;
            expect(updatedSearchPage).toBe(searchPage - 1);
        });
    });

    describe('updatePage', () => {
        beforeAll(() => {
            mockPokemons = buildPokemons(200);
            spyStorage.mockImplementation(() => JSON.stringify(mockPokemons));
            store.dispatch(importPokemons());
        });
        test('updatePage + 1', () => {
            const actualPage = store.getState().api.pokemons.page;
            store.dispatch(updatePage({ page: actualPage + 1 }));
            const updatedPage = store.getState().api.pokemons.page;
            expect(updatedPage).toBe(actualPage + 1);
        });
        test('updatePage - 1', () => {
            const actualPage = store.getState().api.pokemons.page;
            store.dispatch(updatePage({ page: actualPage - 1 }));
            const updatedPage = store.getState().api.pokemons.page;
            expect(updatedPage).toBe(actualPage - 1);
        });
    });
    describe('updatePokemon', () => {
        beforeAll(() => {
            mockPokemons = buildPokemons(200);
            spyStorage.mockImplementation(() => JSON.stringify(mockPokemons));
            store.dispatch(importPokemons());
        });
        test('should update pokemon with new bg url', () => {
            const bgUrl = 'test';
            const pokemonId = 1;
            store.dispatch(
                updatePokemon({
                    pokemons: store.getState().api.pokemons.results,
                    bg: bgUrl,
                    id: pokemonId,
                })
            );
            const pokemon: CardInterface = store.getState().api.pokemons.results[pokemonId];
            expect(pokemon.bg).toBe(bgUrl);
        });
    });
    describe('updateSearch', () => {
        beforeAll(() => {
            mockPokemons = buildPokemons(200);
            spyStorage.mockImplementation(() => JSON.stringify(mockPokemons));
            store.dispatch(importPokemons());
            store.dispatch(lookForPokemon({ search: '1' }));
        });
        test('should update pokemon in search page with new bg url', () => {
            const bgUrl = 'test';
            const pokemonId = 1;
            store.dispatch(
                updateSearch({
                    pokemons: store.getState().api.search.results,
                    bg: bgUrl,
                    id: pokemonId,
                })
            );
            const pokemon: CardInterface = store.getState().api.search.results[0];
            expect(pokemon.bg).toBe(bgUrl);
        });
    });
});
