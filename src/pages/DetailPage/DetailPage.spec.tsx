import { render, screen } from '@testing-library/react';
import DetailPage from './DetailPage';
import * as helper from '../../helpers/helpers';
import { mockPokemonResponse } from '../../../mocks/pokemonMock';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router';
import { act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import axios from 'axios';
import { cleanup } from '@testing-library/react';

import * as Router from 'react-router-dom';

jest.mock('../../components/Moves/Moves', () => {
    return {
        __esModule: true,
        default: () => <section id="moves" data-testid="moves" />,
    };
});

jest.mock('../../components/Stats/Stats', () => {
    return {
        __esModule: true,
        default: () => <section id="stats" data-testid="stats" />,
    };
});

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useParams: jest.fn(() => ({
        id: 10,
    })),
}));

describe('<DetailPage/> page', () => {
    describe('given API call succeed', () => {
        beforeEach(() => {
            jest.spyOn(Router, 'useParams').mockReturnValue({ id: '3' });
        });
        test('should render correctly', async () => {
            jest.spyOn(axios, 'get').mockImplementation((url) => {
                switch (url) {
                    case helper.API_POKEMON_ENDPOINT + mockPokemonResponse.id:
                        return Promise.resolve({ data: mockPokemonResponse });

                    case helper.API_SPECIE_ENDPOINT + mockPokemonResponse.name:
                        return Promise.resolve({
                            data: {
                                habitat: {
                                    name: 'test',
                                },
                            },
                        });
                    default:
                        return Promise.resolve({ data: {} });
                }
            });
            const store = helper.mockedStore();
            act(() => {
                render(
                    <Provider store={store}>
                        <MemoryRouter initialEntries={['/pokemon/' + mockPokemonResponse.id]}>
                            <DetailPage />
                        </MemoryRouter>
                    </Provider>
                );
            });
            expect(screen.queryByTestId('loading')).toBeInTheDocument(); // first we expect to see the loading

            //global container should have data-id matching the params id from useParams
            await screen.findByTestId('pokemon-dp');
            const container = screen.getByTestId('pokemon-dp');
            expect(container).toBeInTheDocument();
            expect(container.getAttribute('data-id')).toMatch(mockPokemonResponse.id.toString());

            //detail container - loading is gone
            await screen.findByTestId('details-container');
            expect(screen.queryByTestId('loading')).not.toBeInTheDocument();
            expect(screen.getByTestId('details-container')).toBeInTheDocument();

            //Previous pokemon link should be there
            const prevPokemonLink = screen.getByTestId('prev-pokemon-link');
            expect(prevPokemonLink).toBeInTheDocument();
            expect(prevPokemonLink.getAttribute('href')).toMatch('/pokemon/2');

            //Next pokemon link sould be there
            const nextPokemonLink = screen.getByTestId('pokemon-next-link');
            expect(nextPokemonLink).toBeInTheDocument();
            expect(nextPokemonLink.getAttribute('href')).toMatch('/pokemon/4');

            //Back to homepage should navigate to /
            expect(screen.queryByTestId('backhome')).toBeInTheDocument();
            userEvent.click(screen.getByTestId('backhome'));
            expect(global.window.location.pathname).toMatch('/');

            //should have ability
            expect(screen.queryAllByTestId('pokemon-ability')).toHaveLength(mockPokemonResponse.abilities.length);

            //Habitat name
            expect(screen.getByTestId('pokemon-habitat')).toBeInTheDocument();
        });

        test('Ability should not be displayed when empty', async () => {
            jest.spyOn(axios, 'get').mockImplementation(() =>
                Promise.resolve({ data: { ...mockPokemonResponse, abilities: {} } })
            );
            const store = helper.mockedStore();
            act(() => {
                render(
                    <Provider store={store}>
                        <MemoryRouter initialEntries={['/pokemon/3']}>
                            <DetailPage />
                        </MemoryRouter>
                    </Provider>
                );
            });

            await screen.findByTestId('details-container'); //detail container - loading is gone
            expect(screen.queryByTestId('loading')).not.toBeInTheDocument();
            expect(screen.getByTestId('details-container')).toBeInTheDocument();
            expect(screen.queryByTestId('pokemon-empty-ability')).toBeInTheDocument();
        });
    });

    describe('Given API Call fails', () => {
        beforeEach(() => {
            jest.spyOn(axios, 'get').mockImplementation(() => Promise.resolve({ data: mockPokemonResponse }));
        });
        test('should render correctly', () => {
            act(() => {
                render(
                    <Provider store={helper.mockedStore()}>
                        <MemoryRouter initialEntries={['/pokemon/' + mockPokemonResponse.id]}>
                            <DetailPage />
                        </MemoryRouter>
                    </Provider>
                );
            });
            screen.findByTestId('pokemon-dp');
            const container = screen.getByTestId('pokemon-dp');
            expect(container).toBeInTheDocument();
            expect(screen.getByTestId('pokemon-empty')).toBeInTheDocument();
        });
    });

    describe('Given a pokemon that is not the last and not the first to be displayed (id = count from api.pokemons - 1)', () => {
        beforeEach(() => {
            jest.spyOn(axios, 'get').mockImplementation(() =>
                Promise.resolve({ data: helper.mockCustomPokemon({ id: 6 }) })
            );
            jest.spyOn(Router, 'useParams').mockReturnValue({ id: '6' });
        });
        afterEach(() => {
            cleanup();
        });
        test('Links to the prev/next pokemon should be displayed', async () => {
            act(() => {
                render(
                    <Provider store={helper.mockedStore()}>
                        <MemoryRouter initialEntries={['/pokemon/6']}>
                            <DetailPage />
                        </MemoryRouter>
                    </Provider>
                );
            });

            //global container
            await screen.findByTestId('pokemon-dp');
            const container = screen.getByTestId('pokemon-dp');
            expect(container).toBeInTheDocument();
            expect(container.getAttribute('data-id')).toMatch('6');
            expect(screen.getByTestId('backhome')).toBeInTheDocument();

            //Previous pokemon link
            const prevPokemonLink = screen.getByTestId('prev-pokemon-link');
            expect(prevPokemonLink).toBeInTheDocument();
            expect(prevPokemonLink.getAttribute('href')).toMatch('/pokemon/5');

            //Next pokemon link
            const nextPokemonLink = screen.getByTestId('pokemon-next-link');
            expect(nextPokemonLink).toBeInTheDocument();
            expect(nextPokemonLink.getAttribute('href')).toMatch('/pokemon/7');
        });
    });

    describe('Given a pokemon that is the last one to be displayed (id = count length from api.pokemons)', () => {
        beforeEach(() => {
            jest.spyOn(axios, 'get').mockImplementation(() =>
                Promise.resolve({ data: helper.mockCustomPokemon({ id: 10 }) })
            );
            jest.spyOn(Router, 'useParams').mockReturnValue({ id: '10' });
        });
        afterEach(() => {
            cleanup();
        });
        test('The link to the next pokemon should not be displayed', async () => {
            act(() => {
                render(
                    <Provider store={helper.mockedStore()}>
                        <MemoryRouter initialEntries={['/pokemon/10']}>
                            <DetailPage />
                        </MemoryRouter>
                    </Provider>
                );
            });

            //global container
            await screen.findByTestId('pokemon-dp');
            const container = screen.getByTestId('pokemon-dp');
            expect(container).toBeInTheDocument();
            expect(container.getAttribute('data-id')).toMatch('10');
            expect(screen.getByTestId('backhome')).toBeInTheDocument();

            expect(screen.queryByTestId('pokemon-next-link')).not.toBeInTheDocument();
        });
    });
});
