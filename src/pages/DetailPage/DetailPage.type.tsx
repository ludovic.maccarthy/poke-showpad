import { Stat } from '../../components/Stats/Stats';

export interface Genre {
    id: number;
    name: string;
}

export interface StatGlobal {
    stat: Stat;
    base_stat: number;
}

export interface Ability {
    ability: {
        name: string;
    };
}

export interface PokeType {
    type: {
        name: string;
    };
}

export interface Move {
    move: {
        name: string;
    };
}

export interface PokemonState {
    id: number;
    name: string;
    stats: Stat[];
    weight: number;
    height: number;
    abilities: Ability[];
    types: PokeType[];
    sprites: {
        front_default: string;
        front_shiny: string;
        other: {
            'official-artwork': {
                front_default: string;
            };
        };
    };
    moves: Move[];
}

export interface PokemonNav {
    name: string;
    id: number;
}
export interface FlavorText {
    flavor_text: string;
    language: {
        name: string;
    };
}
export interface PokeSpecie {
    flavor_text_entries: FlavorText[];
    habitat: {
        name: string;
    };
}
