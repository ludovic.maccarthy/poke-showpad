import { render, screen } from '@testing-library/react';
import * as helper from '../../helpers/helpers';
import Collection from './Collection';

jest.mock('../../components/List/List', () => {
    return {
        __esModule: true,
        default: () => <section id="List" data-testid="List" />,
    };
});

describe('<Collection /> component', () => {
    test('should render correctly', () => {
        render(helper.wrapWithProviderAndRouter(<Collection />));
        expect(screen.getByTestId('collection-container')).toBeInTheDocument();
    });
});
