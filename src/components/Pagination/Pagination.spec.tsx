import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { CardInterface } from '../Card/Card';
import { Pagination } from './Pagination';

describe('<Pagination /> component', () => {
    const defaultProps = {
        type: '',
        page: 2,
        count: 2000,
        results: [],
    };
    const updateFuncMock = jest.fn();
    const cardMock: CardInterface = {
        id: 1,
        name: 'test',
        url: 'test',
        bg: 'test',
    };

    const buildResults = (number: number): CardInterface[] => {
        const results = [];
        let i = 0;
        for (i === 0; i < number; i++) {
            results.push(cardMock);
        }
        return results;
    };

    test('should display 1 stat if props contains 1 stat ', () => {
        const { container } = render(<Pagination {...defaultProps} updateFunc={updateFuncMock} />);
        expect(container.querySelector('[data-testid="pagination-container"]')).toBeInTheDocument();
        expect(container.querySelector('[data-testid="prev-icon"]')).toBeInTheDocument();
        userEvent.click(screen.getByTestId('prev-icon'));
        expect(updateFuncMock).toHaveBeenCalledWith('prev');
    });

    test('should have next icon when more than 20 results', () => {
        const results = buildResults(250);
        const { container } = render(
            <Pagination {...defaultProps} updateFunc={updateFuncMock} results={results} page={1} />
        );
        expect(container.querySelector('[data-testid="next-icon"]')).toBeInTheDocument();
        userEvent.click(screen.getByTestId('next-icon'));
        expect(updateFuncMock).toHaveBeenCalledWith('next');
    });
});
