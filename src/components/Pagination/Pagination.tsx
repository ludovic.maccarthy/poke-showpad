import { CardInterface } from '../Card/Card';
import nextImg from '../../images/arrow_right_btn1.png';
import prevImg from '../../images/arrow_left_btn2.png';

export interface PaginationInterface {
    page: number;
    count: number;
    results: CardInterface[];
    type: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    updateFunc: (...args: any[]) => void;
}

export const Pagination = (props: PaginationInterface) => {
    return (
        <div className="pagination" data-testid="pagination-container">
            {props.page > 1 && (
                <div
                    onClick={() => props.updateFunc('prev')}
                    className="pagination-previous"
                    data-testid="pagination-previous"
                >
                    <img src={prevImg} alt="previmg" data-testid="prev-icon" />
                </div>
            )}
            <div className="pagination-actualpage" data-testid="actual-page">
                {props.page} / {Math.ceil(props.count / 20)}
            </div>
            {props.results.length > 20 && props.page < Math.ceil(props.count / 20) && (
                <div onClick={() => props.updateFunc('next')} className="pagination-next" data-testid="pagination-next">
                    <img src={nextImg} alt="nextimg" data-testid="next-icon" />
                </div>
            )}
        </div>
    );
};
