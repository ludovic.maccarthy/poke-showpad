import { render, screen } from '@testing-library/react';
import Stats from './Stats';

describe('<Stats /> component', () => {
    const maxHeightCss = 'height: 98%;';
    const defaultStats = {
        name: 'test',
        value: 0,
    };

    test('should display 1 stat if props contains 1 stat ', () => {
        const { container } = render(<Stats stats={[defaultStats]} />);
        expect(container.querySelector('#stats')).toBeInTheDocument();
        expect(container.querySelectorAll('.stat')).toHaveLength(1);
    });

    test('should not display stat is props from stats are empty', () => {
        const { container } = render(<Stats stats={[]} />);
        expect(container.querySelectorAll('stat')).toHaveLength(0);
        expect(container.querySelector('[data-testid="no-stats"]')).toBeInTheDocument();
    });

    test('should limit size of jaune if value is > 98%', () => {
        const highStats = {
            name: 'test',
            value: 100,
        };
        render(<Stats stats={[highStats]} />);
        const jauneOn = screen.getByTestId('jaune-on');
        expect(jauneOn.getAttribute('style')).toMatch(maxHeightCss);
    });
});
