import { fireEvent, render, screen } from '@testing-library/react';
import Searchbar from './Searchbar';
import configureStore from 'redux-mock-store';
import * as redux from 'react-redux';
import * as router from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

const initialState = {
    actual_search: '',
};
const mockStore = configureStore();
let store;

describe('<Searchbar /> component', () => {
    test('should render and call useDispatch and useNavigate', () => {
        store = mockStore(initialState);
        const useDispatchSpy = jest.spyOn(redux, 'useDispatch');
        const useNavigateSpy = jest.spyOn(router, 'useNavigate');
        render(
            <redux.Provider store={store}>
                <BrowserRouter>
                    <Searchbar />
                </BrowserRouter>
            </redux.Provider>
        );
        expect(screen.getByTestId('searchbar')).toBeInTheDocument();
        expect(useDispatchSpy).toHaveBeenCalledWith();
        expect(useNavigateSpy).toHaveBeenCalled();
    });

    describe('when trying to search for a pokemon', () => {
        beforeEach(() => {
            store = mockStore(initialState);
            render(
                <redux.Provider store={store}>
                    <BrowserRouter>
                        <Searchbar />
                    </BrowserRouter>
                </redux.Provider>
            );
        });
        test('should display correct input entry and navigate to search with parameters on click search', () => {
            const input = screen.getByTestId('searchbar-input');
            const searchIcon = screen.getByTestId('search-icon');

            fireEvent.change(input, { target: { value: 'a' } });
            expect((input as HTMLInputElement).value).toBe('a');
            userEvent.click(searchIcon);
            expect(global.window.location.search).toContain('?q=a');
        });

        test('should not navigate if search is empty', () => {
            const input = screen.getByTestId('searchbar-input');
            const searchIcon = screen.getByTestId('search-icon');
            fireEvent.change(input, { target: { value: '' } });
            userEvent.click(searchIcon);
            expect(global.window.location.pathname).toMatch('/');
        });

        test('should handle search on keyUp Enter', () => {
            const input = screen.getByTestId('searchbar-input');
            fireEvent.change(input, { target: { value: 'Test' } });
            fireEvent.keyUp(input, {
                key: 'Enter',
                code: 'Enter',
                keyCode: 13,
                charCode: 13,
            });
            expect(global.window.location.search).toMatch('?q=test');
        });
    });
});
