import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import * as helper from '../../helpers/helpers';
import List from './List';
import * as redux from 'react-redux';
import userEvent from '@testing-library/user-event';
import * as apiSlice from '../../features/API/apiSlice';
import * as collectionSlice from '../../features/collection/collectionSlice';
import * as favsSlice from '../../features/favs/favsSlice';

jest.mock('../../components/Card/Card', () => {
    return {
        __esModule: true,
        default: () => <section id="card" data-testid="card" />,
    };
});

describe('<List /> component', () => {
    describe('given page type is pokemons', () => {
        test('should render correctly with type pokemons', async () => {
            const spyDispatch = jest.spyOn(redux, 'useDispatch');
            const pokemons = helper.buildPokemons(100);
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'pokemons'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            expect(screen.getAllByTestId('card')).toHaveLength(20);
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyDispatch).toHaveBeenCalled();
        });

        test('On page 1, click on next page should call updatePage with 2', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdatePage = jest.spyOn(apiSlice, 'updatePage');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'pokemons'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            expect(screen.getAllByTestId('card')).toHaveLength(20);
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyUpdatePage).toHaveBeenCalledWith({ page: 2 });
        });
        test('On page 2, click on prev page should call updatePage with 1', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdatePage = jest.spyOn(apiSlice, 'updatePage');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={2} count={pokemons.length} results={pokemons} type={'pokemons'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            expect(screen.getAllByTestId('card')).toHaveLength(20);
            userEvent.click(await screen.findByTestId('pagination-previous'));
            expect(spyUpdatePage).toHaveBeenCalledWith({ page: 1 });
        });
    });
    describe('given page type is default', () => {
        test('should render correctly with type default', async () => {
            const spyDispatch = jest.spyOn(redux, 'useDispatch');
            const pokemons = helper.buildPokemons(100);
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={''} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            expect(screen.getAllByTestId('card')).toHaveLength(20);
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyDispatch).toHaveBeenCalled();
        });

        test('On page 1, click on next page should call updatePage with 2', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdatePage = jest.spyOn(apiSlice, 'updatePage');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={''} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyUpdatePage).toHaveBeenCalledWith({ page: 2 });
        });
        test('On page 2, click on prev page should call updatePage with 1', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdatePage = jest.spyOn(apiSlice, 'updatePage');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={2} count={pokemons.length} results={pokemons} type={''} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-previous'));
            expect(spyUpdatePage).toHaveBeenCalledWith({ page: 1 });
        });
    });
    describe('given page type is search', () => {
        test('should render correctly with type search', async () => {
            const spyDispatch = jest.spyOn(redux, 'useDispatch');
            const pokemons = helper.buildPokemons(100);
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'search'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            expect(screen.getAllByTestId('card')).toHaveLength(20);
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyDispatch).toHaveBeenCalled();
        });

        test('On page 1, click on next page should call updateSearchPage with 2', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdateSearchPage = jest.spyOn(apiSlice, 'updateSearchPage');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'search'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyUpdateSearchPage).toHaveBeenCalledWith({ page: 2 });
        });
        test('On page 2, click on prev page should call updateSearchPage with 1', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdateSearchPage = jest.spyOn(apiSlice, 'updateSearchPage');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={2} count={pokemons.length} results={pokemons} type={'search'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-previous'));
            expect(spyUpdateSearchPage).toHaveBeenCalledWith({ page: 1 });
        });
    });
    describe('given page type is favorites', () => {
        test('should render correctly with type favorites', async () => {
            const spyDispatch = jest.spyOn(redux, 'useDispatch');
            const pokemons = helper.buildPokemons(100);
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'favorites'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            expect(screen.getAllByTestId('card')).toHaveLength(20);
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyDispatch).toHaveBeenCalled();
        });

        test('On page 1, click on next page should call updatePageFavs with 2', async () => {
            const pokemons = helper.buildPokemons(100);
            const updatePageFavs = jest.spyOn(favsSlice, 'updatePageFavs');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'favorites'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(updatePageFavs).toHaveBeenCalledWith({ page: 2 });
        });
        test('On page 2, click on prev page should call updatePageFavs with 1', async () => {
            const pokemons = helper.buildPokemons(100);
            const updatePageFavs = jest.spyOn(favsSlice, 'updatePageFavs');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={2} count={pokemons.length} results={pokemons} type={'favorites'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-previous'));
            expect(updatePageFavs).toHaveBeenCalledWith({ page: 1 });
        });
    });
    describe('given page type is collection', () => {
        test('should render correctly with type collection', async () => {
            const spyDispatch = jest.spyOn(redux, 'useDispatch');
            const pokemons = helper.buildPokemons(100);
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'collection'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            expect(screen.getAllByTestId('card')).toHaveLength(20);
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyDispatch).toHaveBeenCalled();
        });

        test('On page 1, click on next page should call updatePageCollection with 2', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdatePageCollection = jest.spyOn(collectionSlice, 'updatePageCollection');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={1} count={pokemons.length} results={pokemons} type={'collection'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-next'));
            expect(spyUpdatePageCollection).toHaveBeenCalledWith({ page: 2 });
        });
        test('On page 2, click on prev page should call updatePageCollection with 1', async () => {
            const pokemons = helper.buildPokemons(100);
            const spyUpdatePageCollection = jest.spyOn(collectionSlice, 'updatePageCollection');
            act(() => {
                render(
                    helper.wrapWithProviderAndRouter(
                        <List page={2} count={pokemons.length} results={pokemons} type={'collection'} />
                    )
                );
            });
            expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
            expect(screen.getByTestId('pagination-container')).toBeInTheDocument();
            userEvent.click(await screen.findByTestId('pagination-previous'));
            expect(spyUpdatePageCollection).toHaveBeenCalledWith({ page: 1 });
        });
    });

    test('Pagination should not be in the document', () => {
        const pokemons = helper.buildPokemons(10);
        render(
            helper.wrapWithProviderAndRouter(
                <List page={1} count={pokemons.length} results={pokemons} type={'collection'} />
            )
        );
        expect(screen.getByTestId('pokemons-list-container')).toBeInTheDocument();
        expect(screen.queryByTestId('pagination-container')).not.toBeInTheDocument();
        expect(screen.getAllByTestId('card')).toHaveLength(10);
    });
});
