import { render, screen } from '@testing-library/react';
import Moves from './Moves';

const defaultProps = {
    moves: [{ move: { name: 'test' } }],
};

const emptyProps = {
    moves: [],
};

describe('<Moves /> component', () => {
    test('should display moves block when Moves are not empty', () => {
        render(<Moves {...defaultProps} />);
        expect(screen.getByTestId('moves')).toBeInTheDocument();
    });
    test('should not display moves block when Moves are empty', () => {
        render(<Moves {...emptyProps} />);
        expect(screen.getByTestId('no-moves')).toBeInTheDocument();
    });
});
