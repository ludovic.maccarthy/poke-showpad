import logo from '../../images/logo.png';
import './Header.css';
import { Link } from 'react-router-dom';
import Searchbar from '../Searchbar/Searchbar';
import { Col, Container } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { selectFavs } from '../../features/favs/favsSlice';
import favImg from '../../images/favicon.png';
import collectionImg from '../../images/collection.png';
import { selectCollection } from '../../features/collection/collectionSlice';

function Header() {
    const favs = useSelector(selectFavs);
    const collection = useSelector(selectCollection);

    return (
        <Container fluid className="header d-flex flex-column p-3" data-testid="header">
            <div className="d-flex justify-space-between align-items-center row">
                <Col md={3} xs={12}>
                    <div className="brand d-flex align-items-center justify-space-center">
                        <Link to="/">
                            <img className="logo" src={logo} alt="logoo" />
                        </Link>
                    </div>
                </Col>
                <Col md={6} xs={12}>
                    <nav className="row">
                        <Col md={10} xs={7}>
                            <ul>
                                <li className="menu-link">
                                    <Link to="/favs">
                                        <div className="menu-link_container">
                                            <img src={favImg} alt="favorites img" />
                                            <p data-testid="favoris-counter">
                                                Favorites
                                                {favs.results.length > 0 && ` ( ${favs.results.length} )`}
                                            </p>
                                        </div>
                                    </Link>
                                </li>
                                <li className="menu-link">
                                    <Link to="/collection">
                                        <div className="menu-link_container">
                                            <img src={collectionImg} alt="collection img" />{' '}
                                            <p data-testid="collection-counter">
                                                Collection
                                                {collection.results.length > 0 && ` ( ${collection.results.length} )`}
                                            </p>
                                        </div>
                                    </Link>
                                </li>
                            </ul>
                        </Col>
                    </nav>
                </Col>
                <Col md={3} xs={12}>
                    <Searchbar />
                </Col>
            </div>
        </Container>
    );
}

export default Header;
