import configureStore from 'redux-mock-store';
import { render, screen } from '@testing-library/react';
import Header from './Header';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

const pokeMock = {
    name: 'test',
    id: 1,
    url: 'test',
    bg: 'test',
};

const buildReponse = (count: number) => {
    let i = 0;
    const response = [];
    for (i === 0; i < count; i++) {
        response.push(pokeMock);
    }
    return response;
};

const defaultStoreMock = {
    favs: {
        page: 1,
        results: buildReponse(10),
        count: 10,
        type: 'favorites',
    },
    collection: {
        page: 1,
        results: buildReponse(10),
        count: 10,
        type: 'collection',
    },
};

const mockStore = configureStore();
let store;

describe('<Header /> component', () => {
    beforeEach(() => {
        store = mockStore(defaultStoreMock);
        render(
            <Provider store={store}>
                <BrowserRouter>
                    <Header />
                </BrowserRouter>
            </Provider>
        );
    });
    test('should render', () => {
        expect(screen.getByTestId('header')).toBeInTheDocument();
    });

    test('favoris should have same Number of favs results(store)', () => {
        const favorisCounter = screen.getByTestId('favoris-counter');
        expect(favorisCounter).toHaveTextContent('Favorites ( 10 )');
    });

    test('collection should have same Numer of collection results(store)', () => {
        const favorisCounter = screen.getByTestId('collection-counter');
        expect(favorisCounter).toHaveTextContent('Collection ( 10 )');
    });
});
