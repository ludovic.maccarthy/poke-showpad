import { render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { BrowserRouter } from 'react-router-dom';
import Card from './Card';
import { Provider } from 'react-redux';

const defaultProps = {
    id: 0,
    name: 'test',
    url: 'test',
    bg: 'test',
};

const buildReponse = (count: number) => {
    let i = 0;
    const response = [];
    for (i === 0; i < count; i++) {
        response.push(defaultProps);
    }
    return response;
};

const defaultStoreMock = {
    favs: {
        page: 1,
        results: buildReponse(10),
        count: 10,
        type: 'favorites',
    },
    collection: {
        page: 1,
        results: buildReponse(10),
        count: 10,
        type: 'collection',
    },
};

const mockStore = configureStore();
let store;

describe('<Card /> component', () => {
    test('should render correctly', () => {
        store = mockStore(defaultStoreMock);
        render(
            <Provider store={store}>
                <BrowserRouter>
                    <Card {...defaultProps} />
                </BrowserRouter>
            </Provider>
        );
        expect(screen.getByTestId('pokemon-card')).toHaveAttribute('data-title', 'test');
        expect(screen.getByTestId('pokemon-card-id')).toHaveTextContent('0');
        expect(screen.getByTestId('pokemon-card-title')).toHaveTextContent('test');
        const link = screen.getByRole('link');
        expect(link).toHaveAttribute('href', '/pokemon/0');
    });
});
